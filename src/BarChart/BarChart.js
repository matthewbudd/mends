import React, { Component } from "react";
import * as d3 from "d3";
import "./BarChart.css";

// https://medium.com/@Elijah_Meeks/interactive-applications-with-react-d3-f76f7b3ebc71

class BarChart extends Component {
  constructor(props) {
    super(props);
    this.createBarChart = this.createBarChart.bind(this);
  }

  componentDidMount() {
    this.createBarChart();
  }

  componentDidUpdate() {
    this.createBarChart();
  }

  createBarChart() {
    const node = this.node;
    const dataMax = d3.max(this.props.data.map(d => d.value));
    const svg = d3.select(node);
    //console.log(this.props.data);
    var xScale = d3
      .scaleBand()
      .range([0, this.props.width])
      .padding(0.1);

    const yScale = d3.scaleLinear().range([0, this.props.height]);

    xScale.domain(
      this.props.data.map(function(d, i) {
        return d.name;
      })
    );
    yScale.domain([0, dataMax]);

    var xAxis = d3.axisBottom().scale(xScale);

    var yAxis = d3
      .axisLeft()
      .scale(yScale)
      .ticks(10, "%");

    svg
      .selectAll("rect")
      .data(this.props.data)
      .enter()
      .append("rect");

    svg
      .selectAll("rect")
      .data(this.props.data)
      .exit()
      .remove();

    svg
      .selectAll("rect")
      .data(this.props.data)
      .style("fill", "#fe9922")
      .attr("x", d => {
        return xScale(d.name);
      })
      .attr("y", d => this.props.height - yScale(d.value))
      .attr("height", d => yScale(d.value))
      .attr("width", xScale.bandwidth());

    svg
      .append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + this.props.height + ")")
      .call(xAxis);

    svg
      .append("g")
      .attr("class", "y axis")
      .call(yAxis)
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", -6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Frequency");
  }

  render() {
    return (
      <div className="bar-chart">
        <svg
          ref={node => (this.node = node)}
          width={this.props.width}
          height={this.props.height}
        />
      </div>
    );
  }
}

export default BarChart;
