import React from 'react';
import BarChart from './BarChart.js';

const chartdata = {
  data: [
    { name: "Saturday", value: 15 },
    { name: "Monday", value: 110 },
    { name: "Tuesday", value: 300 },
    { name: "Wednesday", value: 110 },
    { name: "Thursday", value: 40 },
    { name: "Friday", value: 180 }
  ],
  width: 700,
  height: 600
};

export default class Root extends React.Component {
  constructor() {
    super();
    
  }
  componentDidCatch() {
    this.setState({ hasError: true });
  }
  render() {
    return (
      
        <BarChart
          data={chartdata.data}
          width={chartdata.width}
          height={chartdata.height}
        />
    );
  }
}
